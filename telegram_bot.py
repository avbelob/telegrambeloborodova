from telegram.ext import MessageHandler, Filters, Updater
from fandom_parcer import *


class MarvelBot:

    def __init__(self, token):
        self.updater = Updater(token)
        message_handler = MessageHandler(Filters.text | Filters.command, self.handle_message)
        self.updater.dispatcher.add_handler(message_handler)
        self.users = {}

    def run(self):
        self.updater.start_polling()

    def start(self, bot, chat_id):
        bot.sendMessage(chat_id, 'Здравствуйте, меня зовут <i>ДЖАРВИС</i>, я автоматический помощник. '
                                 'Вы можете написать мне что угодно и, если это как-то связано с '
                                 '<b>Кинематографической Вселенной Марвел</b>, я постараюсь рассказать '
                                 'вам об этом немного подробнее. Удачи!\n'
                                 '<i>Для более подобной информации наберите </i><b>/help</b>', 'HTML')
        self.users[chat_id] = 1

    def help(self, bot, chat_id):
        bot.sendMessage(chat_id, 'Я <i>ДЖАРВИС</i>, автоматический помощник. Я могу найти для вас любую(нет) информацию'
                                 ' о <b>Кинематографической Вселенной Марвел</b>. \n Для управления мной вы можете '
                                 'использовать следующие команды:\n<i>/start</i> -запускает и перезапускает '
                                 'меня; \n<i>/help</i> - вызывает данный раздел информации, который может вам '
                                 'помочь; \n<i>/exit</i> - выключает меня до момента, пока я вновь вам не '
                                 'понадоблюсь.\nНадеюсь, данный раздел был полезен для вас, удачного вам пользования!',
                        'HTML')

    def exit(self, bot, chat_id):
        bot.sendMessage(chat_id, 'До свидания!')
        self.users[chat_id] = 0

    def handle_message(self, bot, update):
        chat_id = update.message.chat_id
        message = update.message.text
        if chat_id in self.users:
            if self.users[chat_id] == 0:
                self.start(bot, chat_id)
            elif self.users[chat_id] == 1:
                if message.lower() == '/start':
                    self.start(bot, chat_id)
                elif message.lower() == '/exit':
                    self.exit(bot, chat_id)
                elif message.lower() == '/help':
                    self.help(bot, chat_id)
                else:
                    information = search(message)
                    bot.sendMessage(chat_id, information, 'HTML')
        else:
            self.start(bot, chat_id)
