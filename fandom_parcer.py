from requests import get
from bs4 import BeautifulSoup


def search_reference(beautiful_soup_result):
    if 'Ничего не найдено.' in str(beautiful_soup_result):
        return None
    for paragraph in beautiful_soup_result.findAll('h1'):
        link = paragraph.find('a')
        if link:
            founded_link = link['href']
            return founded_link


def search_for_information(link):
    ret_text = 'Для более подробной информации посетите этот сайт:\n%s' % link
    if link:
        request_by_reference = get(link)
        soup = BeautifulSoup(request_by_reference.text, "lxml")
        found_article = link.split('/')[4]
        found_article = found_article.split('_')
        for paragraph in soup.findAll('p'):
            check_for_paragraph = 1
            i = 0
            word = found_article[i]
            if word not in str(paragraph) and word.lower() not in str(paragraph):
                check_for_paragraph = 0
            while (word in str(paragraph) or word.lower() in str(paragraph)) and i < len(
                    found_article) - 1 and check_for_paragraph == 1:
                i += 1
                word = found_article[i]
                if word not in str(paragraph) and word.lower() not in str(paragraph):
                    check_for_paragraph = 0

            if '<b>' in str(paragraph) and check_for_paragraph:
                ret_text = '%s\n\n%s' % (str(paragraph)[3:len(str(paragraph)) - 4], ret_text)
    else:
        return 'Простите, но по вашему запросу я не смог ничего найти :с'
    return ret_text


def search(request):
    req = get(
        'https://marvelcinematicuniverse.fandom.com/ru/wiki/Служебная:Search?query=' + request)
    soup = BeautifulSoup(req.text, "lxml")
    founded_link = search_reference(soup)
    return search_for_information(founded_link)
